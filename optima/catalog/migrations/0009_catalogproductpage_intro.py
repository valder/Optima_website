# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-26 22:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_catalogindexslidergalleryimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='catalogproductpage',
            name='intro',
            field=models.TextField(default='Додайте короткий опис, для відображення в каталозі', max_length=300, verbose_name='Коротка інформація, для каталогу'),
        ),
    ]
