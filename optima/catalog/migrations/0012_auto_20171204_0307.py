# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-04 03:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0011_auto_20171130_1258'),
    ]

    operations = [
        migrations.AddField(
            model_name='catalogindexpage',
            name='intro',
            field=models.TextField(default='Додайте короткий опис, для відображення в каталозі', max_length=300, verbose_name='Коротка інформація, для каталогу'),
        ),
        migrations.AddField(
            model_name='catalogtilespage',
            name='intro',
            field=models.TextField(default='Додайте короткий опис, для відображення в каталозі', max_length=300, verbose_name='Коротка інформація, для каталогу'),
        ),
        migrations.AlterField(
            model_name='catalogmainpage',
            name='intro',
            field=models.TextField(default='Додайте короткий опис, для відображення в каталозі', max_length=300, verbose_name='Коротка інформація, для каталогу'),
        ),
    ]
