# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-30 12:57
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0009_catalogproductpage_intro'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='catalogmainpage',
            options={'verbose_name': 'Сторінка головного каталогу,                         НЕ СТВОРЮЙТЕ ЦЮ СТОРІНКУ (Головний каталог)'},
        ),
    ]
