from django.db import models

from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager

from taggit.models import TaggedItemBase

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index

from more_itertools import grouper


def is_covidien_related(self):
    answer = False
    for element in self.get_ancestors():
        if 'covi' in str(element.title).lower():
            answer = True
            break
    if 'covi' in str(self.title).lower():
        answer = True
    return answer


def main_image(self):
    gallery_item = self.gallery_images.first()
    if gallery_item:
        return gallery_item.image
    else:
        return None


def rest_of_images(self):
    gallery_items = self.gallery_images.all()[1:]
    if gallery_items:
        return gallery_items
    else:
        return None

def get_slider_image(self):
    slider_image = self.slider_images.first()
    if slider_image:
        return slider_image
    else:
        return None


def get_intro(self, len_of_intro=200):
    if self.intro != 'Додайте короткий опис, для відображення в каталозі':
        response = self.intro
    else:
        if self.description[len_of_intro:len_of_intro+1]:
            response = self.description[0:len_of_intro]+'...'
        else:
            response = self.description
    return response


Page.is_covidien_related = is_covidien_related
Page.main_image = main_image
Page.get_slider_image = get_slider_image
Page.get_intro = get_intro
Page.rest_of_images = rest_of_images

class CatalogIndexPage(Page):
    # Catalog page for listing of all products/sub-dir of manufacturer
    # Restricts from creating it from product page
    parent_page_types = ['catalog.CatalogIndexPage', 'catalog.CatalogMainPage',
                         'catalog.CatalogTilesPage', 'home.HomePage']
    # Allowing creation of next list of child pages
    subpage_types = ['catalog.CatalogIndexPage', 'catalog.CatalogTilesPage',
                     'catalog.CatalogProductPage']
    sphere = models.CharField(blank=False, max_length=90,
                              default='Medicine')
    intro = RichTextField(blank=False,
                          verbose_name='Коротка інформація, для каталогу',
                          max_length=300,
                          default='Додайте короткий опис, для відображення в каталозі')
    description = RichTextField(blank=False)
    short_title = models.CharField(blank=False,
                                   max_length=90,
                                   verbose_name="Ім'я виробника",
                                   default='Manufacturer')

    class Meta:
        verbose_name = "Сторінка каталогу виробника, або можете " \
                       "використати як вкладений каталог (Вкладений каталог)"
    search_fields = Page.search_fields + [
        index.SearchField('description'),
        index.SearchField('short_title'),
        index.SearchField('sphere'),
    ]
    content_panels = Page.content_panels + [
        FieldPanel('sphere', classname="full"),
        FieldPanel('short_title', classname="full"),
        FieldPanel('intro', classname="full"),
        FieldPanel('description', classname="full"),
        InlinePanel('gallery_images', label="Галерея зображень"),
        InlinePanel('slider_images', label="Слайдер на головній")
    ]


class CatalogIndexPageGalleryImage(Orderable):
    page = ParentalKey(CatalogIndexPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )

    panels = [
        ImageChooserPanel('image'),
    ]


class CatalogIndexSliderGalleryImage(Orderable):
    page = ParentalKey(CatalogIndexPage, related_name='slider_images')
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.CASCADE,
                              related_name='+')
    caption = models.CharField(blank=False, max_length=90,
                               default="Insert info, first row")
    small_caption = models.CharField(blank=False, max_length=90,
                                     default="More info, second row")

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption', classname="full"),
        FieldPanel('small_caption', classname="full"),
    ]


"""
"""


class CatalogMainPage(Page):
    # Catalog page for listing of manufacturers, create only one,
    # reuse all future info
    # Restricts from creating inside catalogs and product pages
    parent_page_type = ['home.HomePage']
    subpage_types = ['catalog.CatalogIndexPage']
    intro = RichTextField(blank=False,
                          verbose_name='Коротка інформація, для каталогу',
                          max_length=300,
                          default='Додайте короткий опис, для відображення в каталозі')

    class Meta:
        verbose_name = "Сторінка головного каталогу," \
            "НЕ СТВОРЮЙТЕ ЦЮ СТОРІНКУ (Головний каталог)"
    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname="full")
    ]


"""
"""


class CatalogTilesPage(Page):
    # Disallow creating of main catalog like a child page
    subpage_types = ['catalog.CatalogProductPage', 'catalog.CatalogIndexPage',
                     'catalog.CatalogTilesPage']
    intro = RichTextField(blank=False,
                          verbose_name='Коротка інформація, для каталогу',
                          max_length=300,
                          default='Додайте короткий опис, для відображення в каталозі')
    description = RichTextField(blank=False, verbose_name='Опис розділу',
                                default='ADD DESCRIPTION')

    def simple_pagination(self, elements=3):
        products = self.get_children()
        if len(products) == 0:
            return None
        if len(products) < elements+1:
            return list(grouper(elements, products, fillvalue=None))
        return list(grouper(elements, products, fillvalue=None))

    def animation_time(self, start_time=0.0):
        for _ in range(len(self.simple_pagination())):
            start_time = start_time + 0.2
            yield start_time

    class Meta:
        verbose_name = "Сторінка-каталог у вигляді тайлів, " \
            "для однотипної продукції (Каталог-тайли)"

    search_fields = Page.search_fields + [
        index.SearchField('description'),
    ]
    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        FieldPanel('description', classname="full"),
        InlinePanel('gallery_images', label='Галерея зображень')
    ]


class CatalogTilesPageGalleryImage(Orderable):
    page = ParentalKey(CatalogTilesPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )

    panels = [
        ImageChooserPanel('image'),
    ]


"""
"""


class CatalogProductPage(Page):
    subpage_types = []
    intro = RichTextField(blank=False,
                          verbose_name='Коротка інформація, для каталогу',
                          max_length=300,
                          default='Додайте короткий опис, для відображення в каталозі')
    description = RichTextField(blank=False, verbose_name='Опис продукту')

    class Meta:
        verbose_name = "Кінцева сторінка продукту (Сторінка продукту)"

    search_fields = Page.search_fields + [
        index.SearchField('description'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        FieldPanel('description', classname='full'),
        InlinePanel('gallery_images', label="Галерея зображень")
    ]


class CatalogProductPageGalleryImage(Orderable):
    page = ParentalKey(CatalogProductPage, related_name='gallery_images')
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    panels = [
        ImageChooserPanel('image'),
    ]
