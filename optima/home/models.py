from __future__ import absolute_import, unicode_literals

from django.db import models

from wagtail.wagtailcore.models import Page

from wagtail.wagtailadmin.edit_handlers import (
    FieldPanel, FieldRowPanel,
    InlinePanel, MultiFieldPanel
)
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailforms.models import AbstractEmailForm, AbstractFormField

from modelcluster.fields import ParentalKey


class HomePage(Page):
    #Resctrict from creating inside admin panel and allow only creating main catalog page like a child page
    parent_page_types = []
    subpage_types = ['catalog.CatalogMainPage', 'home.ContactPage']
    is_main_page = True
    pass

class FormField(AbstractFormField):
    page = ParentalKey('ContactPage', related_name='form_fields')


class ContactPage(AbstractEmailForm):
    intro = RichTextField(blank=True,
                          verbose_name="Контакті дані")
    street = models.CharField(null=False, blank=False, max_length=60,
                              verbose_name='Назва вулиці та номер дому')
    city = models.CharField(null=False, blank=False, max_length=60,
                            verbose_name='Місто')
    country = models.CharField(null=False, blank=False, max_length=60,
                               verbose_name='Країна')
    post_index = models.CharField(null=False, blank=False, max_length=10,
                                  verbose_name='Поштовий індекс')
    thank_you_text = RichTextField(blank=True,
                                   verbose_name="Текст після відправки форми")
    phone_number = models.CharField(null=False, blank=False, max_length=20,
                                    verbose_name="Номер телефону")
    mail = models.EmailField(null=False, blank=False, verbose_name='Електронна '
                                                                   'пошта')
    content_panels = AbstractEmailForm.content_panels + [
        FieldPanel('intro', classname="full"),
        FieldPanel('street', classname="full"),
        FieldPanel('city', classname="full"),
        FieldPanel('country', classname="full"),
        FieldPanel('post_index', classname="full"),
        FieldPanel('phone_number', classname="full"),
        FieldPanel('mail', classname="full"),
        InlinePanel('form_fields', label="Form fields"),
        FieldPanel('thank_you_text', classname="full"),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]
